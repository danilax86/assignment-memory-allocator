//
// Created by danila on 1/8/22.
//

#include "test.h"
#include "mem.h"

#define HEAP_SIZE 10
# define MAP_FIXED_NOREPLACE 0x100000 // гыгы

void malloc_test(void *heap_start) {
    puts("Malloc test");
    puts("expected: one block with 1337 bytes of capacity taken");
    void *allocated = _malloc(1337);
    debug_heap(stdout, heap_start);
    puts("Cleanup");
    _free(allocated);
    debug_heap(stdout, heap_start);
    puts("------------------------------------------");
}

void free_one_block_test(void *heap_start) {
    puts("Free one block test");
    puts("expected: one free block with 1337 bytes, 1007 taken and the remaining block is free");
    void *allocated1 = _malloc(1337);
    void *allocated2 = _malloc(1007);
    _free(allocated1);
    debug_heap(stdout, heap_start);
    puts("Cleanup:");
    _free(allocated2);
    debug_heap(stdout, heap_start);
    puts("------------------------------------------");
}

void free_two_blocks_test(void *heap_start) {
    puts("Free two blocks test");
    puts("expected: 1337 taken\n1488 taken\n2031 free (1007 + 1007 + header)\n228 taken");
    void *allocated1 = _malloc(1337);
    void *allocated2 = _malloc(1488);
    void *allocated3 = _malloc(1007);
    void *allocated4 = _malloc(1007);
    void *allocated5 = _malloc(228);
    _free(allocated4);
    _free(allocated3);
    debug_heap(stdout, heap_start);
    puts("Cleanup:");
    _free(allocated5);
    _free(allocated2);
    _free(allocated1);
    debug_heap(stdout, heap_start);
    puts("------------------------------------------");
}

void grow_heap_test(void *heap_start) {
    puts("Grow heap test");
    puts("expected: heap will extend its capacity A LOT");
    void *allocated = _malloc(100000000);
    debug_heap(stdout, heap_start);
    puts("Cleanup:");
    _free(allocated);
    debug_heap(stdout, heap_start);
    puts("------------------------------------------");
}

void grow_heap_new_reg_alloc_test(void *heap_start) {
    puts("Grow heap test with allocating new region");
    puts("expected: empty block of initial heap, taken block is 2 times bigger than size of initial one");
    size_t heap_sz = heap_size(heap_start);
    debug_heap(stdout, heap_start);
    map_pages((uint8_t *) heap_start + heap_sz, 5000, MAP_FIXED_NOREPLACE);
    debug_heap(stdout, heap_start);
    void *allocated = _malloc(heap_sz * 2);
    debug_heap(stdout, heap_start);
    puts("Cleanup:");
    _free(allocated);
    debug_heap(stdout, heap_start);

    puts("                    $$ $$$$$ $$");
    puts("                    $$ $$$$$ $$");
    puts("                   .$$ $$$$$ $$.");
    puts("                   :$$ $$$$$ $$:");
    puts("                   $$$ $$$$$ $$$");
    puts("                   $$$ $$$$$ $$$");
    puts("                  ,$$$ $$$$$ $$$.");
    puts("                 ,$$$$ $$$$$ $$$$.");
    puts("                ,$$$$; $$$$$ :$$$$.");
    puts("               ,$$$$$  $$$$$  $$$$$.");
    puts("             ,$$$$$$'  $$$$$  `$$$$$$.");
    puts("           ,$$$$$$$'   $$$$$   `$$$$$$$.");
    puts("        ,s$$$$$$$'     $$$$$     `$$$$$$$s.");
    puts("      $$$$$$$$$'       $$$$$       `$$$$$$$$$");
    puts("      $$$$$Y'          $$$$$          `Y$$$$$)");
}

void run_tests() {
    void *heap_start = heap_init(HEAP_SIZE);

    malloc_test(heap_start);
    free_one_block_test(heap_start);
    free_two_blocks_test(heap_start);
    grow_heap_test(heap_start);
    grow_heap_new_reg_alloc_test(heap_start);
}
