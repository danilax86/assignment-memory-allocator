//
// Created by danila on 1/8/22.
//

#ifndef TEST_H
#define TEST_H

void malloc_test(void *heap_start);

void free_one_block_test(void *heap_start);

void free_two_blocks_test(void *heap_start);

void grow_heap_test(void *heap_start);

void grow_heap_new_reg_alloc_test(void *heap_start);

void run_tests();

#endif //TEST_H
